import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class implements PageRank algorithm on simple graph structure.
 * Put your name(s), ID(s), and section here.
 *
 */
public class PageRanker {
// P is the set of all pages; |P| = N
// S is the set of sink nodes, i.e., pages that have no out links
// M(p) is the set of pages that link to page p
// L(q) is the number of out-links from page q
// d is the PageRank damping/teleportation factor; use d = 0.85 as is typical
        private static HashMap<Integer,HashSet<Integer>> M = new HashMap<Integer,HashSet<Integer>>();
        private static Map<Integer,Integer> L = new HashMap<Integer,Integer>();
        private static List<Integer> S = new ArrayList<Integer>();
        private static final double d = 0.85;
        private static List<Integer> P = new ArrayList<Integer>();
        private static Map<Integer,Double> PR = new HashMap<Integer,Double>();
        private static int N;
        private int oldPerpex = 0;
        private int countSamePerpex=0;
        private static List<Double> perpex = new ArrayList<>();
        
        //suggested use transverse to map to change map to arraylist to improve further speed, then clear map and use arraylist instead
        
	/**
	 * This class reads the direct graph stored in the file "inputLinkFilename" into memory.
	 * Each line in the input file should have the following format:
	 * <pid_1> <pid_2> <pid_3> .. <pid_n>
	 * 
	 * Where pid_1, pid_2, ..., pid_n are the page IDs of the page having links to page pid_1. 
	 * You can assume that a page ID is an integer.
	 */
	public void loadData(String inputLinkFilename) throws IOException
        {
        /* Get root directory */
            File rootdir = new File(inputLinkFilename);
            if (!rootdir.exists()) {
		System.err.println("Invalid data directory: " + inputLinkFilename);
            }
            try (BufferedReader br = Files.newBufferedReader(Paths.get(inputLinkFilename))) {
                for (String line = null; (line = br.readLine()) != null;) {
                    String spliter[] = line.split("\\s+");
                    HashSet<Integer> setInt = new HashSet<Integer>();
                    for(int i=1;i<spliter.length;i++){
                        int num = Integer.parseInt(spliter[i]);
                        setInt.add(num);
                    }
                    M.put(Integer.parseInt(spliter[0]), setInt);
                    P.add(Integer.parseInt(spliter[0]));
                }
            }
        }
	
	/**
	 * This method will be called after the graph is loaded into the memory.
	 * This method initialize the parameters for the PageRank algorithm including
	 * setting an initial weight to each page.
	 */
	public void initialize()
        {
            //add some that miss
            for(int i=0;i<P.size();i++){
                Object[] mSet = M.get(P.get(i)).toArray();
                for(int j=0;j<mSet.length;j++){
                    if(!M.containsKey(mSet[j])){
                        HashSet<Integer> temp = new HashSet<>();//'cause i want empty but not null
                        M.put((Integer) mSet[j],temp);
                        P.add((Integer) mSet[j]);
                    }
                }
            }
            N=P.size();
            //do L first 
            for(int i:P){
                //assume L is don't have out node
                //// some is repeat so we merge that node // 124093
                //not only L but effect to (M->list)
                if(!L.containsKey(i))
                    L.put(i, 0);
                Set<Integer> listInteger = M.get(i);
                for(int j:listInteger)
                {
                    if(!L.containsKey(j))
                        L.put(j, 1);
                    else
                    {
                        int existed = L.get(j);
                        L.put(j,++existed);        
                    }
                }
            }
            //next find S
            for(int i:P){
                if(L.get(i) == 0)
                {
                    S.add(i);
                }
            }
        }
	
	/**
	 * Computes the perplexity of the current state of the graph. The definition
	 * of perplexity is given in the project specs.
	 */
	public double getPerplexity()
        {
            double power=0; 
            for(int p:PR.keySet()){
               power += PR.get(p) * (Math.log(PR.get(p))/Math.log(2));
            }
            double perplexity = Math.pow(2, -power);
            return perplexity;
        }
	
	/**
	 * Returns true if the perplexity converges (hence, terminate the PageRank algorithm).
	 * Returns false otherwise (and PageRank algorithm continue to update the page scores). 
	 */
	public boolean isConverge()
        {
            double newPerpex = getPerplexity();
            if(countSamePerpex>=3){
                return true;
            }
            if(oldPerpex == (int)newPerpex)
            {
                countSamePerpex++;
            }
            else{
                oldPerpex = (int)newPerpex;
                countSamePerpex = 0;
            }
            perpex.add(newPerpex);
            return false;
        }
	
	/**
	 * The main method of PageRank algorithm. 
	 * Can assume that initialize() has been called before this method is invoked.
	 * While the algorithm is being run, this method should keep track of the perplexity
	 * after each iteration. 
	 * 
	 * Once the algorithm terminates, the method generates two output files.
	 * [1]	"perplexityOutFilename" lists the perplexity after each iteration on each line. 
	 * 		The output should look something like:
	 *  	
	 *  	183811
	 *  	79669.9
	 *  	86267.7
	 *  	72260.4
	 *  	75132.4
	 *  
	 *  Where, for example,the 183811 is the perplexity after the first iteration.
	 *
	 * [2] "prOutFilename" prints out the score for each page after the algorithm terminate.
	 * 		The output should look something like:
	 * 		
	 * 		1	0.1235
	 * 		2	0.3542
	 * 		3 	0.236
	 * 		
	 * Where, for example, 0.1235 is the PageRank score of page 1.
	 * 
	 */
        private static List<Double> newPR = new ArrayList<Double>();
	public void runPageRank(String perplexityOutFilename, String prOutFilename) 
        {
            double value = 1./N;
            for(int p: P){
                PR.put(p,value);
            }
            do{
                double sinkPR = 0;
                for(int p:S){                
                    sinkPR += PR.get(p);
                }
                for(int p:P){
                    double newPRNum = (1. - d)/N;//720939
                    newPRNum += d * sinkPR / N;
                    Set<Integer> pageToP = M.get(p);
                    for(int q:pageToP){
                        //for each page pointing to page p -> M.get(p)
                        newPRNum += d * PR.get(q) / L.get(q);
                                             //^ get index of M at p
                    }
                    newPR.add(newPRNum);
                }
                for(int p=0;p<P.size();p++){
                    PR.put(P.get(p), newPR.get(p));
                }
                newPR.clear();
            }while(!isConverge());
            try {
                //write to File
                writeFile(perplexityOutFilename,prOutFilename);
            } catch (IOException ex) {
                Logger.getLogger(PageRanker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void writeFile(String perplexityOutFilename, String prOutFilename) throws IOException{
            BufferedWriter bw = null;
            File file = new File(perplexityOutFilename);
            if(!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            for(int i=0;i<perpex.size();i++){
                bw.write(perpex.get(i).toString());
                bw.newLine();
            }
            bw.close();
            file = new File(prOutFilename);
            if(!file.exists()) {
                file.createNewFile();
            }
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            for(int i:P){
                bw.write(PR.get(i).toString());
                bw.newLine();
            }
            bw.close();
        }
	
	/**
	 * Return the top K page IDs, whose scores are highest.
	 */
	public Integer[] getRankedPages(int K)
        {
            Map<Integer,Double> sortPR = PR;
            sortPR = sortMap(sortPR);
            int i=0;
            Integer[] topK = new Integer[K];
            for(int sortPRValue:sortPR.keySet())
            {
                if(i>=K)
                    break;
                topK[i] = sortPRValue;
                i++;
            }
            return topK;
        }
	public static <Integer, Double extends Comparable<? super Double>> Map<Integer, Double> sortMap(Map<Integer, Double> map) {
            return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder())).collect(Collectors.toMap(
                Map.Entry::getKey, 
                Map.Entry::getValue, 
                (e1, e2) -> e1,LinkedHashMap::new
              ));
        }
	public static void main(String args[]) throws IOException
	{
	long startTime = System.currentTimeMillis();
		PageRanker pageRanker =  new PageRanker();
		pageRanker.loadData("citeseer.dat");
		pageRanker.initialize();
		pageRanker.runPageRank("perplexity.out", "pr_scores.out");
		Integer[] rankedPages = pageRanker.getRankedPages(100);
	double estimatedTime = (double)(System.currentTimeMillis() - startTime)/1000.0;
		
		System.out.println("Top 100 Pages are:\n"+Arrays.toString(rankedPages));
		System.out.println("Proccessing time: "+estimatedTime+" seconds");
	}
}
